package jp.alhinc.machida_munehiro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		Map<String, String> commodityNames = new HashMap<>();
		Map<String, Long> commoditySales = new HashMap<>();

//		ファイルパスの指定
		String folderPath = args[0];

//		コマンドライン引数が指定されているかチェック
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;

		}

		BufferedReader br = null;
		String readingFileName = "branch.lst";
		if(!input(folderPath, readingFileName, branchNames, branchSales)) {
			return;
		}

		readingFileName = "commodity.lst";
		if(!input(folderPath, readingFileName, commodityNames, commoditySales)) {
			return;
		}



		//以下、ファイル名を全部取得し、判定
		File[] files = new File(folderPath).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length; i++) {
			if(files[i].getName().matches("[0-9]{8}.rcd")) {
				rcdFiles.add(files[i]);
			}
		}

		//連番に並べ替え
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() -1; i++) {
			int format = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			if((latter - format) != 1) {
				System.out.println("売上ファイルが連番になっていません");
				return;
			}
		}


		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				List<String> salesItems = new ArrayList<String>();

				String line;
				while((line = br.readLine()) != null) {
					salesItems.add(line);
				}


				if(salesItems.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}
//				支店コード
				String branchCode = salesItems.get(0);

//				売上ファイルに支店定義ファイルにない支店コードが書かれていた時のエラー
				if(!branchNames.containsKey(branchCode)) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

//				商品コード
				String commodityCode = salesItems.get(1);

//				売上ファイルに商品定義ファイルにない支店コードが書かれていた時のエラー
				if(!commodityNames.containsKey(commodityCode)) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return;
				}


				if(!salesItems.get(2).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
//				支店の売上金額
				long fileSale = Long.parseLong(salesItems.get(2));
				Long saleAmount = branchSales.get(branchCode) + fileSale;

//				商品の売上金額
				long commoditySale = Long.parseLong(salesItems.get(2));
				Long commoditySaleAmount = commoditySales.get(commodityCode) + commoditySale;


				if(saleAmount >= 10000000000L || commoditySaleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

//				支店別に売上金額を加算して追加したMap
				branchSales.put(branchCode, saleAmount);

//				商品別に売上金額を加算して追加したMap
				commoditySales.put(commodityCode, commoditySaleAmount);

			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if(br != null) {
					try {
						br.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

//		ここで支店別の売上ファイル
		String writeingFileName = "branch.out";
		if(!output(folderPath, writeingFileName, branchNames, branchSales)) {
			return;
		}

//		ここで商品別の売上ファイル
		writeingFileName = "commodity.out";
		if(!output(folderPath, writeingFileName, commodityNames, commoditySales)) {
			return;
		}
	}

//	ここからファイルの読み取りメソッド
	public static boolean input(String folderPath, String readingFileName, Map<String, String> nameMap, Map<String, Long> saleMap) {
		BufferedReader br = null;
		try {
			File file = new File(folderPath, readingFileName);

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;


			if(readingFileName.equals("branch.lst")) {
				if(!file.exists()) {
					System.out.println("支店定義ファイルが存在しません");
					return false;
				}

				while((line = br.readLine()) != null) {
					String[] items = line.split(",");
					if((items.length != 2) || (!items[0].matches("[0-9]{3}"))) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return false;
					}

					nameMap.put(items[0], items[1]);
					saleMap.put(items[0], (long)0);
				}

			}else if(readingFileName.equals("commodity.lst")) {
				if(!file.exists()) {
					System.out.println("商品定義ファイルが存在しません");
					return false;
				}

				while((line = br.readLine()) != null) {
					String[] items = line.split(",");
					if((items.length != 2) || (!items[0].matches("^[0-9a-zA-Z]{8}"))) {
						System.out.println("商品定義ファイルのフォーマットが不正です");
						return false;
					}

					nameMap.put(items[0], items[1]);
					saleMap.put(items[0], (long)0);
				}

			}

		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

//	ここからファイルの出力メソッド
	public static boolean output(String folderPath, String writeingFileName, Map<String, String> nameMap, Map<String, Long> saleMap) {

		BufferedWriter bw = null;
		try {
//			今度は書き出す。第２引数にファイル名をいれると、その名前でファイルができる。
			File file = new File(folderPath, writeingFileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key : nameMap.keySet()) {
				bw.write(key + "," + nameMap.get(key) + "," + saleMap.get(key));
				bw.newLine();
			}

		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}

		return true;
	}

}
